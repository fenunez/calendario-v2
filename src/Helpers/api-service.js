import { URL_TURNOS, URL_HORARIOS, REQUESTER_KEY } from "../Helpers/Constants";

export const fetchTurnos = () => dispatch(URL_TURNOS);
export const fetchHorarios = fecha => dispatch(URL_HORARIOS + "/31/189/1/21/" + fecha + "/MASCULINO/1964-10-15");

const dispatch = async (url) => {
    const options = {
      headers: {
        "X-RequesterId": REQUESTER_KEY,
      },
    };
    const response = await fetch(url, options);
    return response.json();
  };