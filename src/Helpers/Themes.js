import { grey, red, yellow, green } from "@material-ui/core/colors";
import { createMuiTheme } from "@material-ui/core";

export const materialTheme = createMuiTheme({
  overrides: {
    MuiPickersBasePicker: {
      pickerView: {
        backgroundColor: "#F0f0f0"
      }
    },
    MuiPickersCalendarHeader: {
      iconButton: {
        display:'none'
      }
    },
    MuiPickersDay: {
      daySelected: { backgroundColor: '#f0f0f0' },
      current: { color: grey["500"] }
    },
  }
});

export const css = {
  root: {
    padding: "7px 0",
    display: "flex",
    justifyContent: "space-evenly",
    backgroundColor: "#f0f0f0"
  },

  card: {
    minWidth: 290,
    backgroundColor: grey["300"]
  },
  
  listSubHeader: {
    fontSize: '1rem',
    lineHeight: '30px'
  },

  listItem: {
    paddingTop:0,
    paddingBottom:0
  },

  listText: {
    marginTop:0,
    marginBottom:0
  },

  calendariosWrapper: {
    flexDirection: "column"
  },
  
  horariosWrapper: {
    flexDirection: "row",
    display: "flex"
  },

  selector:  {
    display: 'flex',
  },

  libre: {
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    color: green[500]
  }
};

// export const ocupado = createMuiTheme({
//   overrides: {
//     MuiPickersDay: {
//       day: { color: red["500"] }
//     },
//     ...bold
//   }
// });

// export const casiOcupado = createMuiTheme({
//   overrides: {
//     MuiPickersDay: {
//       day: { color: yellow["500"] }
//     },
//     ...bold
//   }
// });



// export const temaDelTurno = ({turnosTotales, turnosOcupados}) => {
//   if (turnosTotales === turnosOcupados) return ocupado;
//   if (turnosOcupados >= 1) return casiOcupado;
//   else return disponible;
// }
