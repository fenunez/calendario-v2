import React from "react";
import { parse, format } from "date-fns";
import { es } from "date-fns/locale";
import { STR_DATE_FORMAT, DAYOFWEEK_DATE_FORMAT } from "./Constants";

export const Spacer = (size) => {
  return <div style={{ width: size }} />;
};

export const strDateOfCal = ({day,month,year}) => format(new Date(year,month,day),STR_DATE_FORMAT);
export const stringDateOf = (fecha) => format(fecha, STR_DATE_FORMAT);
export const sinRepetidos = (list) => [...new Set(list)];
export const strDayOfWeekOf = (fecha) =>
format(comparableDateOf(fecha), DAYOFWEEK_DATE_FORMAT, { locale: es });
export const comparableDateOf = (fecha) =>
  parse(fecha, STR_DATE_FORMAT, new Date()).getTime();
export const dateOf = (fecha) =>
  parse(fecha, STR_DATE_FORMAT, new Date());
