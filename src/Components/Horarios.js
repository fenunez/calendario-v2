import React from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { makeStyles } from "@material-ui/styles";
import { css } from "../Helpers/Themes";

const useStyles = makeStyles(css);

const Horarios = ({onInputChange, inputValue, turnos, innerCallbackValue}) => {
  const classes = useStyles();
  const horarioLabel = (turno) => turno.horaInicio + " a " + turno.horaFin;
  // const deberiaDesactivarHorario = (horario) => horario.tomado;

  const handleInputChange = (event,value) => {
    onInputChange(value);
  }

  const onChangeFunc = (e, v, r) => {
    if (r === "select-option") {
      innerCallbackValue(v);
    } 
  };

  return (
    <Autocomplete
      options={turnos}
      className={classes.selector}
      // getOptionDisabled={deberiaDesactivarHorario}
      getOptionLabel={horarioLabel}
      onChange={onChangeFunc}
      inputValue={inputValue}
      onInputChange={handleInputChange}
      renderInput={(params) => (
        <TextField {...params} label="Horarios" variant="outlined" />
      )}
    />
  );
};

export default Horarios;
