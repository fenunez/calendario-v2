import React, { Component } from "react";
import { max } from 'date-fns'

import { Calendar } from "primereact/calendar";
import { css } from "../Helpers/Themes";
import { addMonths, subMonths } from "date-fns";
import { strDateOfCal, dateOf, sinRepetidos } from "../Helpers/Utils";
import { green } from "@material-ui/core/colors";

export class CalendarioComponent extends Component {
  constructor(props) {
    super(props);
    const today = new Date();

    this.state = {
      date: null,
      viewDate: today,
      minDate: today,
      prevSelect: null,
      disabledDays: []
    };

    this.ddates = []
    this.disableDate = this.disableDate.bind(this);
    this.dateTemplate = this.dateTemplate.bind(this);
  }
  
  getTurnoOrUndefined = date => 
  this.props.turnos.find(turno => turno.fecha === strDateOfCal(date))
  
  dateTemplate(date) {
    if(this.props.turnos.length !== 0) {
      const turno = this.getTurnoOrUndefined(date);
      if (turno !== undefined){
        const titulo = "Turnos Disponibles: " + turno.turnosTotales + "\n Turnos Ocupados: " + turno.turnosOcupados
        return <span style={css.libre} data-toggle='tooltip' title={titulo}>{date.day}</span>;
      }
      else if(date.selectable)
      this.disableDate(new Date(date.year,date.month,date.day));
      return date.day;
    } 
  }

  disableDate = (date) => this.ddates.push(date);
  
  disabledDays = () => {
    if(this.props.turnos.length !== 0) {
      const disp = sinRepetidos(this.props.turnos.map(({ fecha }) => dateOf(fecha).getDay()));
      return [0,1,2,3,4,5,6].filter(e => !disp.includes(e))
    }
  }
  
  maxDate = () => {
    if(this.props.turnos.length !== 0) 
      return max(this.props.turnos.map(({fecha}) => dateOf(fecha)))
    
  }

  onChEvent = (e) => {
    this.setState({ date: e.value });
    this.props.callback(e.value);
  };

  monthChange = (e) => {
    if (e.originalEvent.target.classList.contains("p-datepicker-prev-icon"))
      this.setState({ viewDate: subMonths(this.state.viewDate, 3) });
    else this.setState({ viewDate: addMonths(this.state.viewDate, 3) });
  };

  select = ({originalEvent}) => {
    if(!!this.state.prevSelect){
      this.state.prevSelect.style.color = green[500];
    }
  
    const element = originalEvent.target;
    if(element.children.length === 0) {
      this.setState({prevSelect:element});
      element.style.color = 'white';
    }
    else {
      this.setState({prevSelect:element.children[0]});
      element.children[0].style.color = 'white';
    } 
  }

  render() {
    const es = {
      firstDayOfWeek: 1,
      dayNames: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo",],
      dayNamesShort: ["dom", "lun", "mar", "mié", "jue", "vie", "sáb"],
      dayNamesMin: [ "Dom","Lun", "Mar", "Mié", "Jue", "Vie", "Sáb",],
      monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre",],
      monthNamesShort: ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic",],
    };

    return (
      <Calendar
        viewDate={this.state.viewDate}
        onViewDateChange={this.monthChange}
        showOtherMonths={false}
        value={this.state.date}
        inline={true}
        onChange={this.onChEvent}
        onSelect={this.select}
        numberOfMonths={3}
        locale={es}
        minDate={this.state.minDate}
        maxDate={this.maxDate()}
        dateTemplate={this.dateTemplate}
        disabledDays={this.disabledDays()}
        disabledDates={this.ddates}
      />
    );
  }
}
