import React, { useState, useEffect } from "react";
import { ThemeProvider, makeStyles } from "@material-ui/styles";
import Hidden from "@material-ui/core/Hidden";

import "primereact/resources/themes/nova-light/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";

import { CalendarioComponent } from "./CalendarioComponent";
import { armarDisponibilidad } from "./CalendarioTurnosHelper";
import { fetchTurnos, fetchHorarios } from "../Helpers/api-service";
import { materialTheme, css } from "../Helpers/Themes";
import { Spacer, stringDateOf, dateOf } from "../Helpers/Utils";
import Horarios from "./Horarios";
import HorariosDisponibles from "./HorariosDisponibles";

const useStyles = makeStyles(css);

export default function CalendarioTurnos({callbackValue}) {
  const [selectedDate, setSelectedDate] = useState(null);
  const [valorInput, cambiarValorInput] = useState('');
  const [turnos, setTurnos] = useState([]);
  const [horariosDiaElegido, handleHorariosDiaElegido] = useState([]);
  const [disponibilidad, setDisponibilidad] = useState([]);

  const classes = useStyles();

  useEffect(() => {
    async function fetch() {
      const turnos = await fetchTurnos();
      setTurnos(turnos);
      if (turnos !== null) {
        const disponibilidadHoraria = await armarDisponibilidad(turnos);
        setDisponibilidad(disponibilidadHoraria);
      }
    }
    fetch();
  }, []);

  useEffect(() => {
    async function fetch() {
      const esTurnoValido = turnos.some(
        ({ fecha }) => stringDateOf(selectedDate) === fecha
      );
      if (esTurnoValido) {
        const horariosDelTurno = await fetchHorarios(
          stringDateOf(selectedDate)
        );
        handleHorariosDiaElegido(horariosDelTurno);
      }
    }
    if (selectedDate !== null) fetch();
  }, [turnos, selectedDate]);

  return (
    <ThemeProvider theme={materialTheme}>
      <div className={classes.root}>
        <div className={classes.calendariosWrapper}>
          <CalendarioComponent
            callback={setSelectedDate}
            turnos={turnos}
          />
          <Horarios
            inputValue={valorInput}
            onInputChange={cambiarValorInput}
            turnos={horariosDiaElegido}
            innerCallbackValue={callbackValue}
          />
        </div>
        <Spacer />
        <Hidden smDown>
          <HorariosDisponibles horarios={disponibilidad} />
        </Hidden>
      </div>
    </ThemeProvider>
  );
}
