import { temaDelTurno, materialTheme } from "../Helpers/Themes";
import { fetchHorarios } from "../Helpers/api-service";
import { sinRepetidos, strDayOfWeekOf } from "../Helpers/Utils";

// export const buscarTemaODefault = (turnoRenderizable) => {
//   if (turnoRenderizable !== undefined) return temaDelTurno(turnoRenderizable);
//   else return materialTheme;
// };

export const deberiaDesactivarTurnoSi = (turnoAsignable) => {
  return (
    turnoAsignable === undefined ||
    turnoAsignable.noLaborable ||
    turnoAsignable.suspendido
  );
};

export const armarDisponibilidad = async (r) => {
  const t = [];
  const dias = sinRepetidos(r.map(({ fecha }) => strDayOfWeekOf(fecha)));

  for (let dia of dias) {
    const turnoDelDia = r.find(({ fecha }) => strDayOfWeekOf(fecha) === dia);
    const horariosDelTurno = await fetchHorarios(turnoDelDia.fecha);
    t.push({
      diaSemana: dia,
      horario:
        horariosDelTurno[0].horaInicio +
        " - " +
        horariosDelTurno[horariosDelTurno.length - 1].horaFin,
    });
  }
  return t;
};
