import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import Paper from "@material-ui/core/Paper";
import { css } from "../Helpers/Themes";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles(css);

function HorariosDisponibles({ horarios }) {
  const classes = useStyles();

  return (
    <Paper className={classes.card}>
      <Typography style={{ paddingBottom: 10 }} align="center" variant="h6">
        Días y horarios de atención:
      </Typography>
      <List subheader={<li />} dense>
        {horarios.map((sectionId) => (
          <li key={`section-${sectionId.diaSemana}`}>
            <ul>
              <ListSubheader className={classes.listSubHeader}>
                {sectionId.diaSemana.toUpperCase()}
              </ListSubheader>
              
                <ListItem
                  className={classes.listItem}
                  key={`item-${sectionId.diaSemana}-${sectionId.horario}`}
                >
                  <ListItemText
                    className={classes.listText}
                    primary={sectionId.horario}
                  />
                </ListItem>
            </ul>
          </li>
        ))}
      </List>
    </Paper>
  );
}
export default HorariosDisponibles;
